import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServiceBase } from 'src/app/models/service-base';

@Injectable({
  providedIn: 'root',
})
export class ServiceService {

  constructor(private httpClient: HttpClient) { }

  getServices(search: string, status: string): Observable<ServiceBase[]> {
    return this.httpClient.get<ServiceBase[]>(`/rest/v1/services?search=${search}&status=${status}`);
  }
}
