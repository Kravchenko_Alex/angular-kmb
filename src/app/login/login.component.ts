import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthenticationService } from 'src/app/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit, OnDestroy {

  private destroy$: Subject<boolean> = new Subject<boolean>();

  form: FormGroup;
  authError: boolean = false;
  authMessage: string;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private router: Router) {

    this.form = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: [],
    });
    //   this.form = new FormGroup({
    //     'email': new FormControl(this.email),
    //     'password': new FormControl(this.password),
    //   });
  }

  ngOnInit(): void {
    this.rememberMe.setValue(true);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

  login() {
    const val = this.form.value;
    console.log(val.rememberMe);
    if (val.email && val.password) {
      this.authService.login(val.email, val.password)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => {
          localStorage.setItem('globalActiveItem', '1');
          this.router.navigateByUrl('/dashboard/analytics');
        },
        (err: any) => {
          this.authMessage = err;
          this.authError = true;
          this.form.reset();
        },
      );
    }
  }

  get email() {
    return this.form.get('email');
  }

  get password() {
    return this.form.get('password');
  }

  get rememberMe() {
    return this.form.get('rememberMe');
  }
}
