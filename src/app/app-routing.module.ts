import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AnalyticsComponent } from 'src/app/analytics/analytics.component';
import { AuthGuardService as AuthGuard } from 'src/app/auth-guard.service';
import { DashboardComponent } from 'src/app/shared/dashboard/dashboard.component';
import { LoginComponent } from 'src/app/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'dashboard', redirectTo: 'dashboard/analytics', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  {
    path: 'dashboard',
    component: DashboardComponent,
    data: {
      accessRole: new Set<string>(['user']),
    },
    canActivate: [AuthGuard],
    canActivateChild: [AuthGuard],
    children: [
      {
        path: 'analytics',
        component: AnalyticsComponent,
      },
    ],
  },
  {
    path: 'dashboard/slm/services-management',
    loadChildren: './services/services.module#ServicesModule',
    data: {
      accessRole: new Set<string>(['user']),
    },
  },
  {
    path: 'dashboard/pm',
    loadChildren: './party/party.module#PartyModule',
    data: {
      accessRole: new Set<string>(['user']),
    },
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false, // <-- debugging purposes only
    preloadingStrategy: PreloadAllModules,
  })],
})
export class AppRoutingModule {}
