import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/can-deactivate.guard';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit, CanComponentDeactivate {

  constructor() { }

  ngOnInit() {
  }

  canDeactivate(): Observable<boolean> | boolean {
    // Here I should check changes and show popup
    return true;
  }
}
