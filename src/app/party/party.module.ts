import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/auth-guard.service';
import { CanDeactivateGuard } from 'src/app/can-deactivate.guard';
import { ServicesManagementComponent } from 'src/app/services/services-management/services-management.component';
import { DashboardComponent } from 'src/app/shared/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { PartyManagementComponent } from './party-management/party-management.component';
import { UsersManagementComponent } from './users-management/users-management.component';
import { CreateUserComponent } from './create-user/create-user.component';
import { EditUserComponent } from './edit-user/edit-user.component';

const partyRoutes: Routes = [
  {
    path: 'parties',
    component: DashboardComponent,
    data: {
      accessRole: new Set<string>(['user']),
    },
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: PartyManagementComponent,
      },
    ],
  },
  {
    path: 'users',
    component: DashboardComponent,
    data: {
      accessRole: new Set<string>(['user']),
    },
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: UsersManagementComponent,
      },
      {
        path: 'edit-user/:id',
        component: EditUserComponent,
        canDeactivate: [CanDeactivateGuard],
      },
      {
        path: 'create-user/:id',
        component: CreateUserComponent,
        canDeactivate: [CanDeactivateGuard],
      },
    ],
  },
];

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(partyRoutes),
  ],
  declarations: [PartyManagementComponent, UsersManagementComponent, CreateUserComponent, EditUserComponent],
})
export class PartyModule { }
