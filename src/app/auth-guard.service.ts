import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, CanLoad, Route, Router, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from 'src/app/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class AuthGuardService implements CanActivate, CanActivateChild, CanLoad{

  constructor(private authService: AuthenticationService, private router: Router) { }

  canActivate(activatedRoute: ActivatedRouteSnapshot): boolean {
    const accessRoles: Set<string> = new Set(activatedRoute.data.accessRole);

    if (!this.authService.isAuthenticated()) {
      this.router.navigateByUrl('/login');
      return false;
    }
    else if (accessRoles && !this.authService.hasRights(accessRoles)) {
      return false;
    }
    return true;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot): boolean {
    return this.canActivate(childRoute);
  }

  canLoad(route: Route): boolean {
    const accessRoles: Set<string> = new Set(route.data.accessRole);

    if (!this.authService.isAuthenticated()) {
      this.router.navigateByUrl('/login');
      return false;
    }
    else if (accessRoles && !this.authService.hasRights(accessRoles)) {
      return false;
    }
    return true;
  }

}
