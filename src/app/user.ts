export interface User {

  id: number;
  email: string;
  token?: string;
  roles?: string[];
  // constructor(public id: number,
  //             public email: string,
  //             public token?: string,
  //             public roles?: string[],
  // ) {}
}
