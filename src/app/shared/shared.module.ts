import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatBadgeModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatIconModule, MatInputModule,
  MatListModule,
  MatSelectModule, MatTooltipModule,
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { ContentContainerComponent } from 'src/app/shared/dashboard/content-container/content-container.component';
import { DashboardComponent } from 'src/app/shared/dashboard/dashboard.component';
import { MainMenuComponent } from 'src/app/shared/dashboard/main-menu/main-menu.component';
import { MainMenuBlockComponent } from './dashboard/main-menu/main-menu-block/main-menu-block.component';
import { MenuFooterComponent } from './dashboard/main-menu/menu-footer/menu-footer.component';
import { UserBlockComponent } from './dashboard/main-menu/user-block/user-block.component';
import { ToolbarComponent } from './dashboard/toolbar/toolbar.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatSelectModule,
    MatFormFieldModule,
    MatInputModule,
    MatBadgeModule,
    MatTooltipModule,
  ],
  exports: [DashboardComponent, MainMenuComponent, ToolbarComponent],
  declarations: [
    DashboardComponent,
    ContentContainerComponent,
    MainMenuComponent,
    MainMenuBlockComponent,
    UserBlockComponent,
    MenuFooterComponent,
    ToolbarComponent,
  ],
})
export class SharedModule {}
