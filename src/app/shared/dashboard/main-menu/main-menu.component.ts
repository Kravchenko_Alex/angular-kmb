import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-menu',
  templateUrl: './main-menu.component.html',
  styleUrls: ['./main-menu.component.scss'],
})
export class MainMenuComponent implements OnInit {

  globalActiveItem: number;

  constructor() { console.log('Its reloads every time');}

  ngOnInit() {
    this.globalActiveItem = JSON.parse(localStorage.getItem('globalActiveItem'));
  }

  onSelect(event: number) {
    console.log(event);
    this.globalActiveItem = event;
    localStorage.setItem('globalActiveItem', JSON.stringify(event));
  }

  monitoringRoutes: { route: string, name: string, index: number }[] = [
    { route: 'dashboard/analytics', name: 'Analytics', index: 1 }];
  reportRoutes: { route: string, name: string, index: number }[] = [
    { route: 'dashboard/slm/services-management', name: 'Services', index: 2 }];
}
