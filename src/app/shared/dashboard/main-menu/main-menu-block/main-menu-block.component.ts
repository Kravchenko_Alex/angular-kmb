import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-main-menu-block',
  templateUrl: './main-menu-block.component.html',
  styleUrls: ['./main-menu-block.component.scss'],
})
export class MainMenuBlockComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input() header: string;
  @Input() routesAndNames: {route: string, name: string, index: number}[];
  @Input() globalActiveItem: number;
  @Output() selectEvent = new EventEmitter<number>();

  select(index: number): void {
    this.selectEvent.emit(index);
  }
}
