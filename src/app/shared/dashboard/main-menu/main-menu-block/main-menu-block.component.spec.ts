import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainMenuBlockComponent } from './main-menu-block.component';

describe('MainMenuBlockComponent', () => {
  let component: MainMenuBlockComponent;
  let fixture: ComponentFixture<MainMenuBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MainMenuBlockComponent],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainMenuBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
