import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
})
export class ToolbarComponent implements OnInit {

  @Output() toggleFilters = new EventEmitter();
  @Input() pageName: string = null;
  @Input() filterItemsSelected: number;

  filterPanelOpen: boolean = false;

  constructor() {
    this.filterItemsSelected = 1;
  }

  ngOnInit() {
  }

  toggleFilterPanel() {
    this.toggleFilters.emit();
    this.filterPanelOpen = !this.filterPanelOpen;
  }
}
