import { Action } from '@ngrx/store';
import { ServiceBase } from 'src/app/models/service-base';

export const SELECT_SERVICE = '[Service] Select';
export const ADD_SERVICE = '[Service] Add';
export const LOAD_SERVICES = '[Service] Load';
export const LOAD_SERVICES_SUCCESS = '[Service] Load Success';

export class SelectService implements Action {
  readonly type = SELECT_SERVICE;

  constructor(public payload: number) { }
}

export class AddService implements Action {
  readonly type = ADD_SERVICE;

  constructor(public payload: ServiceBase) { }
}

export class LoadServices implements Action {
  readonly type = LOAD_SERVICES;

  constructor(public search: string, public status: string) { }
}

export class LoadServicesSuccess implements Action {
  readonly type = LOAD_SERVICES_SUCCESS;

  constructor(public payload: ServiceBase[]) { }
}

export type Action = AddService | SelectService | LoadServices | LoadServicesSuccess;
