import { Injectable } from '@angular/core';
import { Actions, Effect } from '@ngrx/effects';
import { map, switchMap } from 'rxjs/operators';
import { ServiceService } from 'src/app/service.service';
import * as serviceActions from 'src/app/store/action/services';

@Injectable()
export class ServiceEffect {
  constructor(
    private actions$: Actions,
    private serviceService: ServiceService,
  ) {}

  @Effect()
  loadServices$ = this.actions$.ofType(serviceActions.LOAD_SERVICES).pipe(
    switchMap((action: serviceActions.LoadServices) => {
      return this.serviceService
      .getServices(action.search, action.status)
      .pipe(
        map(services => new serviceActions.LoadServicesSuccess(services)),
      );
    }),
  );

}
