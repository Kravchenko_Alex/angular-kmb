import { ServiceBase } from 'src/app/models/service-base';
import * as serviceActions from 'src/app/store/action/services';

export interface State {
  ids: number[];
  services: { [id: number]: ServiceBase };
  selected: number;
}

export const initialState: State = {
  ids: [],
  services: {},
  selected: null,
};

export function reducer(state = initialState, action: serviceActions.Action): State {
  switch (action.type) {
    case serviceActions.ADD_SERVICE: {
      const newService: ServiceBase = action.payload;

      return {
        ids: [...state.ids, newService.id],
        services: Object.assign({}, state.services, {
          [newService.id]: newService,
        }),
        selected: state.selected,
      };
    }

    case serviceActions.SELECT_SERVICE: {
      const id = action.payload;
      return {
        ...state,
        selected: id,
      };
    }

    case serviceActions.LOAD_SERVICES_SUCCESS: {
      const services: ServiceBase[] = action.payload;
      const newServices = services.filter(service => !state.services[service.id]);

      const newServiceIds = newServices.map(service => service.id);
      const newStoreServiceEntities = newServices.reduce((storeEntity: { [id: string]: ServiceBase }, service: ServiceBase) => {
        return Object.assign(storeEntity, {
          [service.id]: service,
        });
      }, {});

      return {
        ids: [...state.ids, ...newServiceIds],
        services: Object.assign({}, state.services, newStoreServiceEntities),
        selected: state.selected,
      };
    }

    default:
      return state;
  }
}

export const getIds = (state: State) => state.ids;
export const getServices = (state: State) => state.services;
export const getSelectedId = (state: State) => state.selected;
