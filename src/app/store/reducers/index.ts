import {
  ActionReducerMap,
  createSelector,
  createFeatureSelector,
  ActionReducer,
  MetaReducer,
} from '@ngrx/store';

import * as fromServices from './services';

export interface State {
  services: fromServices.State;
}

export const reducers = {
  services: fromServices.reducer,
};

export function logger(reducer: ActionReducer<State>): ActionReducer<State> {
  return function (state: State, action: any): State {
    console.log('state', state);
    console.log('action', action);
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<State>[] = [logger];

//

export const getServicesState = createFeatureSelector<fromServices.State>('services');

export const getServicesIds = createSelector(
  getServicesState,
  fromServices.getIds,
);

export const getServices = createSelector(
  getServicesState,
  fromServices.getServices,
);

export const getSelectedServiceId = createSelector(
  getServicesState,
  fromServices.getSelectedId,
);

export const getSelectedService= createSelector(
  getSelectedServiceId,
  getServices,
  (selectedId, services) => {
    return {
      ...services[selectedId],
    };
  }
);

export const getAllServices = createSelector(
  getServicesIds,
  getServices,
  (ids, services) => {
    return ids.map(id => services[id]);
  },
);
