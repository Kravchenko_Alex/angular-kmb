export class ServiceBase {
  id: number;
  name: string;
  status: string;
  type: ServiceType;
  currentServiceStatus: string;
}

class ServiceType {
  id: number;
  name: string;
  code: string;
}
