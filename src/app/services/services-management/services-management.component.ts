import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDrawer, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ServiceBase } from 'src/app/models/service-base';
import * as serviceActions from 'src/app/store/action/services';
import * as rootReducer from '../../store/reducers';

@Component({
  selector: 'app-services-management',
  templateUrl: './services-management.component.html',
  styleUrls: ['./services-management.component.scss'],
})
export class ServicesManagementComponent implements OnInit {

  services$: Observable<ServiceBase[]>;

  columnNames: string[] = ['Name', 'Status', 'Type', 'Service Status'];
  columnElementField:  string[] = ['name', 'status', 'type.name', 'currentServiceStatus'];
  columnsToDisplay: string[] = ['select'].concat(this.columnNames.slice());
  dataSource = null;
  selection = new SelectionModel<ServiceBase>(true, []);

  @ViewChild('filtersPanel') filtersPanel: MatDrawer;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private store: Store<rootReducer.State>) {
  }

  ngOnInit() {
    this.store.dispatch(new serviceActions.LoadServices('', ''));
    this.services$ = this.store.select(rootReducer.getAllServices);
    this.services$.subscribe((services) => { this.dataSource = new MatTableDataSource(services); });

    this.dataSource.sort = this.sort;
  }

  toggleFilters() {
    this.filtersPanel.toggle();
  }

  /** Whether the number of selected elements matches the total number of rows. */
  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  gotoEntity(row: any) {
    console.log(row.position);
    // this.router.navigateByUrl(`/dashboard/slm/service/${row.position})`);
  }

}
