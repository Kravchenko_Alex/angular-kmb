import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {
  MatCheckboxModule, MatChipsModule,
  MatExpansionModule,
  MatFormFieldModule, MatIconModule,
  MatInputModule,
  MatListModule, MatPaginatorModule,
  MatSidenavModule, MatSortModule, MatTableModule
} from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from 'src/app/auth-guard.service';
import { DashboardComponent } from 'src/app/shared/dashboard/dashboard.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { ServicesManagementComponent } from './services-management/services-management.component';

const servicesRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    data: {
      accessRole: new Set<string>(['user']),
    },
    canActivateChild: [AuthGuard],
    children: [
      {
        path: '',
        component: ServicesManagementComponent,
      },
    ],
  },
];

const MATERIAL_MODULES = [
  MatCheckboxModule,
  MatSidenavModule,
  MatFormFieldModule,
  MatInputModule,
  MatExpansionModule,
  MatListModule,
  MatChipsModule,
  MatIconModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
];

@NgModule({
  exports: [RouterModule],
  imports: [
    CommonModule,
    ...MATERIAL_MODULES,
    SharedModule,
    RouterModule.forChild(servicesRoutes),
  ],
  declarations: [ServicesManagementComponent],
})
export class ServicesModule { }
