import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { ServiceBase } from 'src/app/models/service-base';
import { User } from 'src/app/user';

@Injectable()
export class FakeServerInterceptor implements HttpInterceptor {

  private usersOnServer: User[] = [
    { id: 11, email: 'wislatest@wellink.ru', roles: ['user'] },
    { id: 12, email: 'multimetro@wellink.ru', roles: ['admin'] },
    { id: 13, email: 'smartdev@wellink.ru' },
  ];

  private servicesOnServer: ServiceBase[] = [
    {
      id: 1,
      name: 'Simple 1',
      currentServiceStatus: 'GREEN',
      status: 'ACTIVE',
      type: { id: 11, code: 'data_channel', name: 'Канал связи' },
    },
    {
      id: 2,
      name: 'Simple 2',
      currentServiceStatus: 'YELLOW',
      status: 'ACTIVE',
      type: { id: 11, code: 'data_channel', name: 'Канал связи' },
    },
    {
      id: 3,
      name: 'Simple 3',
      currentServiceStatus: 'RED',
      status: 'ACTIVE',
      type: { id: 11, code: 'data_channel', name: 'Канал связи' },
    },
    {
      id: 4,
      name: 'Simple 4',
      currentServiceStatus: 'BLACK',
      status: 'ACTIVE',
      type: { id: 11, code: 'data_channel', name: 'Канал связи' },
    },
  ];

  constructor() {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return of(null).pipe(mergeMap(() => {
      if (request.url.endsWith('/login') && request.method === 'POST') {

        const matchUsers = this.usersOnServer.filter((user) => {
          return user.email === request.body.email && request.body.password === 'GfHjkm!@';
        });

        if (matchUsers.length) {
          const user = matchUsers[0];
          user.token = 'fake-token';

          return of(new HttpResponse({ status: 200, body: user }));
        }
        else {
          // Return error response that will be catched by ErrorInterceptor
          return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }
      }

      if (request.url.startsWith('/rest/v1/services') && request.method === 'GET') {
        return of(new HttpResponse({ status: 200, body: this.servicesOnServer }));
      }
    }));
  }
}
