import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { User } from 'src/app/user';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  constructor(private httpClient: HttpClient) { }

  login(email: string, password: string) {
    return this.httpClient.post<User>('api/login', { email, password })
    .pipe(map((user) => {
        // login successful if there's token in the response
      if (user && user.token) {
        localStorage.setItem('currentUser', JSON.stringify(user));
      }
      return user;
    }),
          // catchError((error: any): Observable<any> => {
          //   return of(null);
          // }),
    );
  }

  getCurrentUser(): User {
    if (this.isAuthenticated()) {
      return <User> JSON.parse(localStorage.getItem('currentUser'));
    }
    return null;
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  isAuthenticated(): boolean {
    const user = localStorage.getItem('currentUser');
    // Check is user`s token not expire
    return !!user;
  }

  hasRights(neededRoles: Set<string>): boolean {
    const user:User = JSON.parse(localStorage.getItem('currentUser'));
    const userRoles: Set<string> = new Set<string>(user.roles);
    if (user && userRoles && neededRoles
      && AuthenticationService.isSuperset(userRoles, neededRoles)) {
      return true;
    }
    return false;
  }

  private static isSuperset(set:Set<string>, subset:Set<string>) {
    for (const elem of Array.from(subset)) {
      if (!set.has(elem)) {
        return false;
      }
    }
    return true;
  }
}
