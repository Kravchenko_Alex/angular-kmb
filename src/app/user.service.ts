import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { User } from 'src/app/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {

  private users: User[] = [
    { id: 11, email: 'wislatest@wellink.ru', roles: ['user'] },
    { id: 12, email: 'multimetro@wellink.ru', roles: ['admin'] },
    { id: 13, email: 'smartdev@wellink.ru' },
  ];

  private usersUrl = 'api/users';

  constructor(
    private http: HttpClient,
  ) {}

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.log(error); // log to console
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getUser(id: number): Observable<User> {
    return of(this.users[1]);
  }

  // getAll(): Observable<User[]> {
  //   console.log('In User Service');
  //   return this.http.get<User[]>(this.usersUrl)
  //   .pipe(
  //     tap(() => console.log('All user fetched')),
  //     catchError(this.handleError('getAll', [])),
  //   );
  // }
  //
  // getUser(id: number): Observable<User> {
  //   const url = `${this.usersUrl}/${id}`;
  //   return this.http.get<User>(url).pipe(
  //     tap(() => console.log(`fetched user id=${id}`)),
  //     catchError(this.handleError<User>(`getHero id=${id}`)),
  //   );
  // }
}
